# README #

### Pour installer l'application :
 * Cloner ce repo.
 * Importer ce code sous Eclipse ou équivalent en tant que AspectJ Project

### Pour tester l'application :
 * Lancer le main de la classe Client du package client.
Ce package client contient deja un squelette d'application à tester.

 * Ou bien rajouter des classes java dans ce repo et lancer la classe contenant un main.
Il suffit de les rajouter n'importe ou dans le code tant qu'il est à l'intérieur du package vvproject.

### Contenu de l'application
 * Le package client contenant un sample d'application à tester avec une classe Client qui est launcher.
 * Le package trace qui contient l'aspect Java permettant de tracer l'exécution du programme contenu dans le package client (mais aussi ailleurs si vous décidez de créer des classes à tester vous même par exemple).
 * Le package graph contenant la gestion du graphe.