package fr.istic.vvproject.client.controller;

import fr.istic.vvproject.client.model.Engine;
import fr.istic.vvproject.client.model.EngineImpl;
import fr.istic.vvproject.client.model.exception.ValueOutOfBoundException;
import fr.istic.vvproject.client.view.ControlButton;
import fr.istic.vvproject.client.view.Led;
import fr.istic.vvproject.client.view.MesureButton;
import fr.istic.vvproject.client.view.Speakers;
import fr.istic.vvproject.client.view.TempoCursor;
import fr.istic.vvproject.client.view.TempoText;
import fr.istic.vvproject.client.view.implementation.DecButton;
import fr.istic.vvproject.client.view.implementation.IncButton;
import fr.istic.vvproject.client.view.implementation.LedImpl;
import fr.istic.vvproject.client.view.implementation.SpeakersImpl;
import fr.istic.vvproject.client.view.implementation.StartStopButton;
import fr.istic.vvproject.client.view.implementation.TempoCursorImpl;
import fr.istic.vvproject.client.view.implementation.TempoTextImpl;

/**
 * The controller of the application.
 * Manipulates the model {@link Engine}.
 *
 * @author CADORET Ronan, GUILLOU Quentin, JEGU Yann
 * @version 1.0
 */
public class Controller {

    private Engine model;
    private ControlButton start;
    private ControlButton stop;
    private MesureButton incButton;
    private MesureButton decButton;
    private TempoCursor tempoSlider;
    private TempoText tempoText;
    private Led ledTempo;
    private Led ledMesure;
    private Speakers speaker;

    /**
     * Constructor of the controller.
     * Adds commands to the model and to the GUI.
     * @param model the reference to the model corresponding to the engine.
     * @param ihm the reference to the view corresponding to the JavaFX GUI.
     */
    public Controller(Engine model) {
        this.model = model;
        this.start = new StartStopButton();
        this.stop = new StartStopButton();
        this.incButton = new IncButton();
        this.decButton = new DecButton();
        this.tempoSlider = new TempoCursorImpl();
        this.tempoText = new TempoTextImpl();
        this.ledTempo = new LedImpl();
        this.ledMesure = new LedImpl();
        this.speaker = new SpeakersImpl();
    }

    /**
     * Updates the state of the button in the view which allows to start or stop the metronome.
     */
    void updateStart() {
        boolean started = this.model.isStarted();
    }

    /**
     * Updates the number of beats in a measure.
     */
    void updateMesure() {
        int mesure = model.getNbMesure();
    }

    /**
     * Updates the tempo, that is to say the speed between two beats in a measure.
     */
    void updateTempo(){
        int tempo = this.model.getTempo();
    }

    /**
     * Calls the start method inside the model.
     * @see EngineImpl#start()
     */
    void start(){
        try {
            this.model.start();
        } catch (ValueOutOfBoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Calls the stop method inside the model.
     * @see EngineImpl#stop()
     */
    void stop(){
        this.model.stop();
    }

    /**
     * Manipulates the model {@link EngineImpl} to increment the number of beats per measure.
     * @see EngineImpl#getNbMesure()
     * @see EngineImpl#setNbMesure(int)
     */
    void increment(){
        int measure = model.getNbMesure() + 1;
        try {
            model.setNbMesure(measure);
        } catch (ValueOutOfBoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Manipulates the model {@link EngineImpl} to decrement the number of beats per measure.
     * @see EngineImpl#getNbMesure()
     * @see EngineImpl#setNbMesure(int)
     */
    void decrement(){
        int measure = model.getNbMesure() - 1;
        try {
            model.setNbMesure(measure);
        } catch (ValueOutOfBoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when the value of the slider in the view has changed.
     * It manipulates the model {@link EngineImpl} when the state of the slider changes.
     * @see EngineImpl#setTempo(int)
     */
    void onSliderChange() {
        int tempo = 30;
        try {
            model.setTempo(tempo);
        } catch(ValueOutOfBoundException voobe){
            voobe.printStackTrace();
        }
    }
}
