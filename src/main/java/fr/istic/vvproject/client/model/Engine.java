package fr.istic.vvproject.client.model;

import fr.istic.vvproject.client.model.exception.ValueOutOfBoundException;

/**
 * Interface of the model of the application.
 *
 * @author CADORET Ronan, GUILLOU Quentin, JEGU Yann
 * @version 1.0
 */
public interface Engine {
    int MEASURE_MIN = 2;
    int MEASURE_MAX = 7;
    int TEMPO_MIN = 3;
    int TEMPO_MAX = 300;

    /**
     * Starts the metronome.
     * @throws ValueOutOfBoundException if the tempo is invalid on start (negative values).
     */
    void start() throws ValueOutOfBoundException;

    /**
     * Stops the metronome.
     */
    void stop();

    /**
     * Retrieves the current tempo of the metronome.
     * @return the current tempo.
     */
    int getTempo();

    /**
     * Updates the tempo of the metronome.
     * @param tempo the new value of the tempo.
     * @throws ValueOutOfBoundException If the new tempo is outside minimum or maximum bounds.
     */
    void setTempo(int tempo) throws ValueOutOfBoundException;

    /**
     * Retrieves the current number of beats per measure of the metronome.
     * @return the current number of beats per measure
     */
    int getNbMesure();

    /**
     * Updates the number of beats per measure of the metronome.
     * @param mesure the new number of beats per measure.
     * @throws ValueOutOfBoundException If the number of beats per measure is outside the minimum or maximum bounds.
     */
    void setNbMesure(int mesure) throws ValueOutOfBoundException;

    /**
     * Adds command in the map of commands.
     * @param type the type of command.
     * @param cmd the command.
     * @see TypeCommand
     * @see Command
     */
    void addCmd(TypeCommand type, Command cmd);

    /**
     * Checks if the metronome is on or not.
     * @return true if the metronome has started, false otherwise.
     */
    boolean isStarted();

    /**
     * Activates a command at each period 'period' (milliseconds).
     * @param period the period in which the command will be executed.
     * @throws ValueOutOfBoundException If the period has a negative value.
     * @throws NullPointerException If the command cmd is null or the map which contains the commands is null.
     */
    void activatePeriodically(float period) throws ValueOutOfBoundException, NullPointerException;

    /**
     * Activates a command after a delay 'delay' (milliseconds).
     * @param delay the delay in which the command will be executed.
     * @throws ValueOutOfBoundException If the period has a negative value.
     * @throws NullPointerException If the command cmd is null or the map which contains the commands is null.
     */
    void activateAfterDelay(float delay) throws ValueOutOfBoundException, NullPointerException;

    /**
     * Desactivates a command.
     * It will remove the command from the map.
     * And also will remove the {@link java.util.Timer} associated to the command.
     * @param cmd the key corresponding to the command to delete in the map.
     */
    void desactivate(TypeCommand cmd);

    /**
     * Desactivates all commands from the map.
     * It will also remove the {@link java.util.Timer} associated to the command.
     */
    void desactivateAll();
}
