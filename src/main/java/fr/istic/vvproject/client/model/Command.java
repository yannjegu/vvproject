package fr.istic.vvproject.client.model;

public interface Command {
    void execute();
}
