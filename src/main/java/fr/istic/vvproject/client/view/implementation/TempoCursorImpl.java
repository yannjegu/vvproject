package fr.istic.vvproject.client.view.implementation;

import fr.istic.vvproject.client.view.TempoCursor;

/**
 * Element of the GUI which corresponds to the slider to update the tempo.
 * @see Slider
 * @see TempoCursor
 *
 * @author CADORET Ronan, GUILLOU Quentin, JEGU Yann
 * @version 1.0
 */
public class TempoCursorImpl implements TempoCursor {
    /**
     * Retrieves the value of the slider.
     * @return the int value of the slider.
     */
    public int getValueSlider() {
    	return 0;
    }
}
