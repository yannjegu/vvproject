package fr.istic.vvproject.client.view;

public interface Led {
    void flashLedOnTempo();
    void flashLedOffTempo();
    void flashLedOnMesure();
    void flashLedOffMesure();
}
