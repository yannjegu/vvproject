package fr.istic.vvproject.client.view.implementation;

import fr.istic.vvproject.client.view.MesureButton;

/**
 * Element of the GUI which corresponds to the button to increment the number of beats per measure.
 * @see Button
 * @see MesureButton
 *
 * @author CADORET Ronan, GUILLOU Quentin, JEGU Yann
 * @version 1.0
 */
public class IncButton implements MesureButton {
    /**
     * Update CSS of the button to render a button clickable.
     */
    public void setClickable() {
    	
    }

    /**
     * Update CSS of the button to render a button unclickable.
     */
    public void setUnclickable() {

    }
}
