package fr.istic.vvproject.client.view;

import java.util.HashMap;
import java.util.Map;

import fr.istic.vvproject.client.model.Command;
import fr.istic.vvproject.client.model.TypeCommand;

/**
 * View of the metronome.
 */
public class IHM {

    private Map<TypeCommand, Command> commands;

    /**
     * Constructor of IHM
     */
    public IHM() {
        commands = new HashMap<TypeCommand, Command>();
    }

    /**
     * Adds command to this view.
     * @param type the type of the command (the key) to add in the map of commands.
     * @param cmd the command to add.
     */
    public void addCmd(TypeCommand type, Command cmd){
        if (this.commands == null) return;
        this.commands.put(type, cmd);
    }

    /**
     * Called when slider change its state.
     */
    void onSliderChange(){
        this.commands.get(TypeCommand.SIG_UPDATE_SLIDER).execute();
    }

    /**
     * Called when the button start is pressed.
     */
    void start(){
        this.commands.get(TypeCommand.SIG_START).execute();
    }

    /**
     * Called when the button stop is pressed.
     */
    void stop(){
        this.commands.get(TypeCommand.SIG_STOP).execute();
    }

    /**
     * Called when the button increment is pressed.
     */
    void increment(){
        this.commands.get(TypeCommand.SIG_INC_MESURE).execute();
    }

    /**
     * Called when the button decrement is pressed.
     */
    void decrement(){
        this.commands.get(TypeCommand.SIG_DEC_MESURE).execute();
    }
}
