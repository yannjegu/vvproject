package fr.istic.vvproject.client.view.implementation;

import fr.istic.vvproject.client.view.Led;

/**
 * Element of the GUI which corresponds to the LEDs to visualize the beats of a mesure and the tempo.
 * @see Circle
 * @see Led
 *
 * @author CADORET Ronan, GUILLOU Quentin, JEGU Yann
 * @version 1.0
 */
public class LedImpl implements Led {


    /**
     * Constructor.
     */
    public LedImpl() {
        super();
    }

    /**
     * Update CSS to simulate the tempo LED flashing on.
     */
    public void flashLedOnTempo() {
    	
    }

    /**
     * Update CSS to simulate the tempo LED flashing off.
     */
    public void flashLedOffTempo() {
    	
    }

    /**
     * Update CSS to simulate the measure LED flashing on.
     */
    public void flashLedOnMesure() {
    	
    }

    /**
     * Update CSS to simulate the measure LED flashing off.
     */
    public void flashLedOffMesure() {
    	
    }
}
