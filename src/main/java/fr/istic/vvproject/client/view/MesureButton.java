package fr.istic.vvproject.client.view;

public interface MesureButton {
    void setUnclickable();
    void setClickable();
}
