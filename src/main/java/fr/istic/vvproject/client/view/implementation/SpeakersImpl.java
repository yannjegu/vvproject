package fr.istic.vvproject.client.view.implementation;

import fr.istic.vvproject.client.view.Speakers;

/**
 * SpeakersImpl
 * Created by quentin on 08/01/17.
 */
public class SpeakersImpl implements Speakers {

    /**
     * Constructor of SpeakersImpl
     */
    public SpeakersImpl() {
    	
    }

    /**
     * Play the sound we want in the metronome.
     */
    public void playSound() {
    	
    }
}
