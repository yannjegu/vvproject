package fr.istic.vvproject.client.view;

public interface TempoCursor {
    int getValueSlider();
}
