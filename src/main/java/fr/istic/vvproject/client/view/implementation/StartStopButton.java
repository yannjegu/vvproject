package fr.istic.vvproject.client.view.implementation;

import fr.istic.vvproject.client.view.ControlButton;

/**
 * Element of the GUI which corresponds to the button to start or stop the metronome.
 * @see Button
 * @see ControlButton
 *
 * @author CADORET Ronan, GUILLOU Quentin, JEGU Yann
 * @version 1.0
 */
public class StartStopButton implements ControlButton {
    /**
     * Allows to display or not the button start (or stop).
     * @param visible
     */
    public void setDisplay(boolean visible) {

    }
}
