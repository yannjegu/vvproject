package fr.istic.vvproject.client.view;

import fr.istic.vvproject.client.controller.Controller;
import fr.istic.vvproject.client.model.Engine;
import fr.istic.vvproject.client.model.EngineImpl;
import fr.istic.vvproject.client.view.implementation.DecButton;
import fr.istic.vvproject.client.view.implementation.IncButton;
import fr.istic.vvproject.client.view.implementation.LedImpl;
import fr.istic.vvproject.client.view.implementation.SpeakersImpl;
import fr.istic.vvproject.client.view.implementation.StartStopButton;
import fr.istic.vvproject.client.view.implementation.TempoCursorImpl;
import fr.istic.vvproject.client.view.implementation.TempoTextImpl;

/**
 * The entry point of the application.
 * It extends the {@link Application} class of JavaFX.
 */
public class InterfaceImpl {

    public static final int DEFAULT_TEMPO = 30;
    public static final int DEFAULT_MEASURE = 4;

    /**
     * Method called by JavaFX on starting the application
     * @param primaryStage the main stage of the GUI.
     * @throws Exception
     */
    public void start() throws Exception {

        IHM ihm = new IHM();
        Engine engine = new EngineImpl(DEFAULT_TEMPO, DEFAULT_MEASURE);
        Controller controller = new Controller(engine);

        ControlButton start = new StartStopButton();
        ControlButton stop = new StartStopButton();
        MesureButton inc = new IncButton();
        MesureButton dec = new DecButton();
        Led ledMesure = new LedImpl();
        Led ledTempo = new LedImpl();
        TempoCursor tempoSlider = new TempoCursorImpl();
        TempoText tempoText = new TempoTextImpl();
        Speakers speakers = new SpeakersImpl();


    }
}