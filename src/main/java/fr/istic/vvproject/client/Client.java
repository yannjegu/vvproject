package fr.istic.vvproject.client;

import fr.istic.vvproject.client.controller.Controller;
import fr.istic.vvproject.client.model.Engine;
import fr.istic.vvproject.client.model.EngineImpl;
import fr.istic.vvproject.client.model.exception.ValueOutOfBoundException;
import fr.istic.vvproject.client.view.MesureButton;
import fr.istic.vvproject.client.view.Speakers;
import fr.istic.vvproject.client.view.implementation.IncButton;
import fr.istic.vvproject.client.view.implementation.SpeakersImpl;

/**
 * Launcher and class monitored by the graph
 *
 */
public class Client {
	public static void main(String[] args) throws InterruptedException, NullPointerException, ValueOutOfBoundException {
		Square square = new Square(4, 4, 40, 40);
		String s = new String("ok");
		String s2 = new String("o");
		s.indexOf(0);
		s.length();
		square.getX();
		square.getY();
		Circle c = square.getCircle();
		c.getRayon();
		
		Engine e = new EngineImpl(30, 4);
		Controller controller = new Controller(e);
		Speakers sp = new SpeakersImpl();
		MesureButton mb = new IncButton();
	}
}