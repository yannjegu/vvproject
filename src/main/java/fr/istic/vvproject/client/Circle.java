package fr.istic.vvproject.client;

/**
 * Test class used to show the graph
 *
 */
public class Circle {
	private int rayon;
	
	public int getRayon() {
		return rayon;
	}

	public void setRayon(int rayon) {
		this.rayon = rayon;
	}

	public Circle(int rayon) {
		this.rayon = rayon;
	}
}
