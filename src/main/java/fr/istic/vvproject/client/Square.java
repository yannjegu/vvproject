package fr.istic.vvproject.client;

/**
 * Test class used to show the graph
 *
 */
public class Square {
	private int x;
	private int y;
	private int length;
	private int height;
	private Circle circle;
	public Square(int x, int y, int length, int height) {
		this.x = x;
		this.y = y;
		this.length = length;
		this.height = height;
		this.circle = new Circle(5);
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getLength() {
		return length;
	}
	
	public void setLength(int length) {
		this.length = length;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public String toString() {
		String chain = "";
		chain += "x: " + this.x + "\n";
		chain += "y: " + this.y + "\n";
		chain += "length: " + this.length + "\n";
		chain += "height: " + this.height + "\n";
		return chain;
	}

	public Circle getCircle() {
		return circle;
	}

	public void setCircle(Circle circle) {
		this.circle = circle;
	}
}
