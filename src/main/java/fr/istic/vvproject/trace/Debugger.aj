package fr.istic.vvproject.trace;

import fr.istic.vvproject.graph.GraphUtil;

public aspect Debugger {
	
	// Reference to the graph
	private GraphUtil graphUtil;
	
	// The main class name
	private static String ENTRY_CLASS_NAME = Thread.currentThread().getStackTrace()[2].getClassName();
	
	
	/**
	 * Constructor of Debugger
	 * Initialize the GraphUtil
	 */
	public Debugger() {
		this.graphUtil = new GraphUtil();
		
		try {
			Class<?> clazz = Class.forName(ENTRY_CLASS_NAME);
			ENTRY_CLASS_NAME = clazz.getSimpleName();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		this.graphUtil.createInitialNode(ENTRY_CLASS_NAME);
	}
	
	/**
	 * Before call to an instantiation
	 * Call to create an node
	 * Ignore the Debugger and GraphUtil instantiated object
	 */
    before() : call(*.new(..)) && !within(Debugger) && !within(GraphUtil) {
    	String currentObjectType = thisJoinPoint.getSignature().getDeclaringType().getSimpleName();
    	String callerClass = Thread.currentThread().getStackTrace()[2].getClassName();
    	Class<?> clazz = null;
		try {
			clazz = Class.forName(callerClass);
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
    	String currentClassName = clazz.getSimpleName();
    	this.graphUtil.instantiate(currentClassName, currentObjectType);
    }
    
    /**
     * Before call to a method
     * Call to create an edge between the objects
     * Ignore the Debugger and GraphUtil called methods
     */
    before() : call(* *(..)) && !within(Debugger) && !within(GraphUtil) {
    	String currentMethodName = thisJoinPoint.getSignature().getName();
    	String currentClassName = thisJoinPoint.getSignature().getDeclaringType().getSimpleName();
    	String callerClass = Thread.currentThread().getStackTrace()[Thread.currentThread().getStackTrace().length - 1].getClassName();
    	Class<?> clazz = null;
		try {
			clazz = Class.forName(callerClass);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
    	String callerClassName = clazz.getSimpleName();
    	
    	Object[] o = thisJoinPoint.getArgs();
    	for(int i = 0; i < o.length; i++){
    		System.out.println(o[i].toString());
    	}
    	try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	this.graphUtil.displayMethodCall(callerClassName, currentClassName, currentMethodName);
    }
}