package fr.istic.vvproject.graph;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

public class GraphUtil {
	
	private Graph graph;
	
	// The time to wait between two methods
	private static final int TIME = 1000;
	
	/**
	 * Constructor of GraphUtil
	 * Initialize the Graph
	 */
	public GraphUtil(){
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		graph = new SingleGraph("VVGraph");
		graph.addAttribute("ui.stylesheet", "url(file:stylesheet)");
		graph.addAttribute("ui.antialias");
		graph.addAttribute("ui.quality");
		graph.addAttribute("layout.stabilization-limit", 200);
		
		graph.display();
	}
	
	/**
	 * createInitialNode
	 * Create the initial node without edge
	 * @param nodeName the node name
	 */
	public synchronized void createInitialNode(String nodeName){
		try{
			graph.addNode(nodeName);
			graph.getNode(nodeName).addAttribute("ui.label", nodeName);
			graph.getNode(nodeName).addAttribute("ui.style", "stroke-color: red;");
			Thread.sleep(TIME);
			graph.getNode(nodeName).addAttribute("ui.style", "stroke-color: black;");
		}
		catch(InterruptedException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * instantiate
	 * Create the edge between the origin and the newClass nodes
	 * @param origin the origin object name
	 * @param newClass the new object name
	 */
	public synchronized void instantiate(String origin, String newClass){
		Node node = graph.getNode(origin);
		if(node == null) {
			return;
		}
		if(graph.getNode(newClass) != null) {
			return;
		}
		
		try{
			graph.addNode(newClass);
			graph.addEdge(node+newClass, origin, newClass, true);
			
			graph.getNode(newClass).addAttribute("ui.label", graph.getNode(newClass));
			
			graph.getEdge(node+newClass).addAttribute("ui.style", "fill-color: red; size: 2px;");
			graph.getNode(newClass).addAttribute("ui.style", "stroke-color: red;");
			graph.getEdge(node+newClass).addAttribute("ui.label", "create()");
			Thread.sleep(TIME);
			graph.getEdge(node+newClass).addAttribute("ui.style", "fill-color: black; size: 1px;");
			graph.getNode(newClass).addAttribute("ui.style", "stroke-color: black;");
			graph.getEdge(node+newClass).removeAttribute("ui.label");
		}
		catch(InterruptedException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * displayMethodCall
	 * Add the method name on the edge
	 * @param node1 the origin node name
	 * @param node2 the destination node name
	 * @param methodName the method name
	 */
	public synchronized void displayMethodCall(String node1, String node2, String methodName){
		if(graph.getEdge(node1+node2) == null) return;
		
		try{
			graph.getEdge(node1+node2).addAttribute("ui.style", "fill-color: blue; size: 2px;");
			graph.getEdge(node1+node2).addAttribute("ui.label", methodName+"()");
			Thread.sleep(TIME);
			graph.getEdge(node1+node2).addAttribute("ui.style", "fill-color: black; size: 1px;");
			graph.getEdge(node1+node2).removeAttribute("ui.label");
		}
		catch(InterruptedException e){
			e.printStackTrace();
		}
		
	}

	public int getNumberOfNodes() {
		return this.graph.getNodeCount();
	}

}
